const winston = require('winston');

const opt = {
  file: {
    level: 'info',
    filename: './logs/app.log',
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  console: { 
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: false
  }
};

const logger = winston.createLogger({
  transports: [
    new winston.transports.File(opt.file),
    new winston.transports.Console(opt.console)
  ],

  exitOnError: false
});

module.exports = logger;
