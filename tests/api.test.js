'use strict';

const request = require('supertest');

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');

const app = require('../src/app')(db);
const buildSchemas = require('../src/schemas');

describe('API tests', () => {
  before((done) => {
    db.serialize((err) => {
      if (err) {
        return done(err);
      }

      buildSchemas(db);

      done();
    });
  });

  describe('GET /health', () => {
    it('should return health', (done) => {
      request(app)
        .get('/health')
        .expect('Content-Type', /text/)
        .expect(200, done);
    });
  });

  describe('POST /rides', () => {
    it('return start_lat < -90', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: -100,
          start_long: 20,
          end_lat: 89,
          end_long: 179,
          rider_name: 'testname',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return start_lat > 90', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 91,
          start_long: 20,
          end_lat: 89,
          end_long: 179,
          rider_name: 'testname',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return start_long < -180', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 10,
          start_long: -190,
          end_lat: 89,
          end_long: 179,
          rider_name: 'testname',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return start_long > 180', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 10,
          start_long: 190,
          end_lat: 89,
          end_long: 179,
          rider_name: 'testname',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return end_lat < -90', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 20,
          start_long: 20,
          end_lat: -92,
          end_long: 179,
          rider_name: 'testname',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return end_lat > 90', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 20,
          start_long: 20,
          end_lat: 96,
          end_long: 179,
          rider_name: 'testname',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return end_long < -180', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 20,
          start_long: 20,
          end_lat: 30,
          end_long: -190,
          rider_name: 'testname',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return end_long > 180', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 20,
          start_long: 20,
          end_lat: 20,
          end_long: 190,
          rider_name: 'testname',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return riderName is null or not string', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 20,
          start_long: 20,
          end_lat: 20,
          end_long: 20,
          rider_name: '',
          driver_name: 'testdriver',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return driverName is null or not string', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 20,
          start_long: 20,
          end_lat: 20,
          end_long: 20,
          rider_name: 'test',
          driver_name: '',
          driver_vehicle: 'test'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return driverVehicle is null or not string', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 20,
          start_long: 20,
          end_lat: 20,
          end_long: 20,
          rider_name: 'test',
          driver_name: 'testDriver',
          driver_vehicle: ''
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('create ride', (done) => {
      request(app)
        .post('/rides')
        .send({
          start_lat: 20,
          start_long: 20,
          end_lat: 20,
          end_long: 20,
          rider_name: 'test',
          driver_name: 'testDriver',
          driver_vehicle: 'testVehicle'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });

  describe('GET /rides', () => {
    it('return rides ', (done) => {
      request(app)
        .get('/rides')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('return specific rides ', (done) => {
      request(app)
        .get('/rides/1')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });

  // Test SQL Injection
  describe('GET /rides/:id', () => {
    const input = '1 OR 1=1';
    it('should response with 404: Rides not found', (done) => {
      request(app)
        .get(`/rides/${input}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(404)
        .expect(function (res) {
          if (res.body.length > 1) throw new Error('Vulnerable to SQL injection');
        })
        .end((err) => {
          if (err) return done(err);
          done();
        });
    });
  });
});
